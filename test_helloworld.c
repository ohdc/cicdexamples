/* Programa: Hola mundo */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <check.h>


// The function we want to test
int compare_strings(const char *str1, const char *str2) {
    return strcmp(str1, str2) == 0 ? 1 : 0;
}

const char *  helloworld(){
	//return "Hello World from function!!!\n";
	return "Hello World from function!!!\n";
}

// Test case for the compare_strings function
START_TEST(test_compare_strings) {
    // Test with equal strings
    //ck_assert_int_eq(compare_strings("hello", "hello"), 1);
    //ck_assert_int_eq(compare_strings("hello", helloworld()), 1);
    ck_assert_int_eq(compare_strings("Hello World from function!!!\n", helloworld()), 1);
    // Test with different strings
    ck_assert_int_eq(compare_strings("hello", "world"), 0);
    // Test with one empty string
    ck_assert_int_eq(compare_strings("", ""), 1);
}
END_TEST


// Test suite definition
Suite *test_suite() {
    Suite *s = suite_create("StringComparison");
    TCase *tc_core = tcase_create("Core");
    tcase_add_test(tc_core, test_compare_strings);
    suite_add_tcase(s, tc_core);
    return s;
}





int main() {
    // Initialize the test suite
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    
    // Run the tests
    srunner_run_all(sr, CK_NORMAL);
    
    // Get the number of failures
    int num_failed = srunner_ntests_failed(sr);
    
    // Clean up
    srunner_free(sr);
    
    // Return 0 if all tests passed, otherwise return the number of failed tests
    return (num_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}





/*


int main()
{
//    printf( "Hola mundo." );
	printf("%s",  helloworld());
	//printf("%d", compare_strings("uno","uno"));
	printf("%d", compare_strings("Hello World from function!!!\n", helloworld()));

	printf("%s", "\n");
    return 0;
}


*/
